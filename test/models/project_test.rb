require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  test "should not save project without name" do
    project = Project.new
    assert_not project.save, "Saved the project without a name" 
  end

  test "should not save project without a git_url" do
    project = Project.new
    assert_not project.save, "Saved the project without a git_url"
  end

  test "should not save project without a git_id" do 
    project = Project.new
    assert_not project.save, "Saved the project without a git_id"
  end
end
