Rails.application.routes.draw do
  apipie
  namespace :api do
    namespace :v1 do
      resources :projects do 
        resources :commits, only: [:index]
        resources :users, only: [:index]
        
        get '/commits/simplediffs',to: 'commits#simplediffs'
        get '/commits/statistics', to: 'commits#statistics'
        get '/commits/diffs',      to: 'commits#diffs'
        get '/commits/sentiments', to: 'commits#sentiments'
        post '/mycommits',         to: 'commits#mycommits'
        
        
        get '/emails/statistics',  to: 'emails#statistics'
        get '/emails',             to: 'emails#emails'
        post '/myemails',          to: 'emails#myEmails'

        get '/ircs',               to: 'ircs#ircs'
        get '/ircs/statistics',    to: 'ircs#statistics'
        post '/ircs/mymesssages',   to: 'ircs#myMessages'

      end
    end
  end
  root to: 'application#main'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
