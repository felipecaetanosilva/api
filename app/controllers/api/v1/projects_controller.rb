require 'rest-client'

class Api::V1::ProjectsController < ApplicationController
    before_action :set_project, only: [:show, :update, :destroy]
    # GET /projects

    api :GET, '/projects', 'List projects'
    returns :code => 200 do 
        property :id, :number, desc: 'Id of the Project'
        property :project_name, String, desc: 'Name of the project'
        property :git_url, String, desc: 'Url of the git repository of the project'
        property :git_id, :number, desc: 'Id of the git project in the microservice'
    end
    def index
        if params['random']
            @projects = Project.order('RANDOM()')
        else
            @projects = Project.all()
        end

        if params['limit']
            @projects = @projects.last(params['limit'])
        end
        
        render json: @projects, status: :ok
    end    

    api :GET, '/projects/:id', 'Read a project'
    returns :code => 201 do 
        property :id, :number, desc: 'Id of the Project'
        property :project_name, String, desc: 'Name of the project'
        property :git_url, String, desc: 'Url of the git repository of the project'
        property :git_id, :number, desc: 'Id of the git project in the microservice'
    end
    # GET /projects/1
    def show
        render json: @project, status: :ok 
    end

    api :POST, '/projects', 'Create a project'
    returns :code => 201 do 
        property :id, :number, desc: 'Id of the Project'
        property :project_name, String, desc: 'Name of the project'
        property :git_url, String, desc: 'Url of the git repository of the project'
        property :git_id, :number, desc: 'Id of the git project in the microservice'
        property :irc_server, String, desc: 'The server name of the ir channel'
        property :irc_channel, String, desc: 'The channel name'

    end
    def create
        @project = Project.new(project_params)
        rid = save_to_commit_bot @project
        @project.git_id = rid
        save_to_irc_bot @project
        if @project.save
            render json: @project, status: :created, location: api_v1_project_url(@project)
        else 
            render json: @project.errors, status: :unprocessable_entity
        end
    end

    # PATCH/PUT /projects/1
    api :PATCH, '/projects/:id', 'Update a project'
    api :PUT, '/projects/:id', 'Update a project'
    returns :code => 200 do 
        property :id, :number, desc: 'Id of the Project'
        property :project_name, String, desc: 'Name of the project'
        property :git_url, String, desc: 'Url of the git repository of the project'
        property :git_id, :number, desc: 'Id of the git project in the microservice'
    end
    def update
        if @project.update(project_params)
            render json: @project, status: :ok
        else
            render json: @project.errors, status: :unprocessable_entity
        end
    end
    
    # DELETE /projects/1
    api :DELETE, '/projects/:id', 'Remove a project'
    returns :code => 204 do 
        property :id, :number, desc: 'Id of the Project'
        property :project_name, String, desc: 'Name of the project'
        property :git_url, String, desc: 'Url of the git repository of the project'
        property :git_id, :number, desc: 'Id of the git project in the microservice'
    end
    def destroy
        destroyed_project = @project
        begin
            response = RestClient.delete ENV['BASE_URL'] + "commits/api/v1/repositories/" + @project.git_id.to_s
            if(response.code == 200)
                @project.destroy
                render json: destroyed_project, status: 204
            else
                render "Can't destroy your commit repository"
            end
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response, status: 204
        end
    end

    private
    def set_project
        @project = Project.find(params[:id])
    end
    
    def project_params
        params.require(:project).permit(:project_name, :git_url, :git_id, :irc_channel, :irc_server)
    end
    
    def save_to_commit_bot(this_project)
        commit_body = { name: this_project.project_name, url: this_project.git_url }.to_json
        begin
            response = RestClient.post ENV['BASE_URL'] + "commits/api/v1/repositories", commit_body
            rid = JSON.parse(response)['rid']
            return rid
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end

    def save_to_irc_bot(this_project)
        irc_body    = { server: this_project.irc_server, channel: this_project.irc_channel }.to_json
        begin 
            response = RestClient.post ENV['BASE_URL'] + "irc/register", irc_body
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end

    def destroy_commit_repo(this_project)
        begin
            response = RestClient.delete ENV['BASE_URL'] + "commits/api/v1/repositories/" + this_project.git_id.to_s
            return true
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response, status: 204
        end
    end

    def destroy_irc_channel(this_project)
        irc_body    = { server: this_project.irc_server, channel: this_project.irc_channel }.to_json
        begin 
            response = RestClient.post ENV['BASE_URL'] + "irc/unregister", irc_body
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end
end
