require 'rest-client'

class Api::V1::CommitsController < ApplicationController
    before_action :set_project, only: [:index, :statistics, :diffs, :sentiments, :mycommits, :simplediffs]
    before_action :user_email, only: [:mycommits]

    # GET /projects/1/commits
    api :GET, '/projects/:project_id/commits', 'Get an array with all the commits of the project'
    returns :code => 200 do 
        property :id, :number, desc: 'Id of the commit'
        property :date, String, desc: 'Date of the commit'
        property :author_email, String, desc: 'Email of the author'
        property :commiter_name, String, desc: 'Name of the commiter'
        property :subject, String, desc: 'Subject of the commit message'

    end
    def index
        begin
            puts @project.git_url
            if @project.git_url == "https://github.com/git/git" 
                data = read_template
                render json: data
            else    
                commits_response = RestClient.get ENV['BASE_URL'] + "commits/api/v1/commits?rid=#{@project.git_id}"
                response = commits_response.body
                render json: response, status: :ok 
            end
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end


    api :POST, '/projects/:project_id/mycommits', 'Get an array with all the commits of the author email'
    returns :code => 200 do 
        property :id, :number, desc: 'Id of the commit'
        property :date, String, desc: 'Date of the commit'
        property :author_email, String, desc: 'Email of the author'
        property :commiter_name, String, desc: 'Name of the commiter'
        property :subject, String, desc: 'Subject of the commit message'
    end
    def mycommits
        begin
            response = RestClient.get ENV['BASE_URL'] + "commits/api/v1/mycommits?rid=#{@project.git_id}&uemail=#{user_email["author_email"]}"
            render json: response.body, status: :ok 
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end

    # GET /projects/1/commits/statistics
    api :GET, '/projects/:project_id/commits/statistics',  'Get statistics of the git repository'
    returns :code => 200 do 
        property :total_commits, :number, desc: 'Total number of commits from the reposotory'
        property :distinct_members, :number, desc: 'Number of contributors'
        property :total_by_member, Hash, desc: 'List of every contributor and its number of contributions'
    end
    def statistics
        begin
            if @project.git_url == "https://github.com/git/git" 
                hash = read_template
            else    
                commits = RestClient.get ENV['BASE_URL'] + "commits/api/v1/commits?rid=#{@project.git_id}"
                hash = JSON.parse commits
            end
            total_distinct = 0;
            contributors = hash.group_by{|e| e["author_email"]}.map{|k, v| [k, v.length]}.to_h
            hash.group_by{|e| e["author_email"]}.each{|k, v| total_distinct = total_distinct + 1}.to_h
            response = {
                :total_commits => hash.count,
                :distinct_members => total_distinct,
                :total_by_member => contributors,
            }
            render json: response , status: :ok 
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end      
    end

    # GET /projects/1/commits/diffs
    api :GET, '/projects/:project_id/commits/diffs',  'Get an array representing users and its own commits,
                                                       with informations about the dates and number of altered lines'
    returns :code => 200 do 
        property :name, String
        property :commits, Array[:commits]
        property "commits[n].date", String
        property "commits[n].author_name", String
        property "commits[n].added_lines", :number
        property "commits[n].deleted_lines", :number
    end                                                   
    def diffs
        begin
            if @project.git_url == "https://github.com/git/git" 
                original = read_diffs
            else    
                commits = RestClient.get ENV['BASE_URL'] + "commits/api/v1/commits/diffs?rid=#{@project.git_id}"
                original = JSON.parse commits
            end
            hash = original.group_by{|e| e["author_name"]}
            newArray = []
            hash.keys.each do |key| 
                entry = {
                    name: key,
                    commits: hash[key],
                }
                newArray << entry
            end
            render json: newArray , status: :ok        
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end

    # GET /projects/1/commits/simplediffs
    api :GET, '/projects/:project_id/commits/simplediffs',  'Get an array representing users and its own commits,
                                                       with informations about the dates and commiter email'
    returns :code => 200 do 
        property :name, String
        property :commits, Array[:commits]
        property "commits[n].date", String
        property "commits[n].author_email", String
        property "commits[n].committer_email", String
    end                                                   
    def simplediffs
        begin
                
            commits = RestClient.get ENV['BASE_URL'] + "commits/api/v1/commits/simplediffs?rid=#{@project.git_id}"
            original = JSON.parse commits
            
            hash = original.group_by{|e| e["author_email"]}
            newArray = []
            hash.keys.each do |key| 
                entry = {
                    name: key,
                    commits: hash[key],
                }
                newArray << entry
            end
            render json: newArray , status: :ok        
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end

    private
    def set_project
        @project = Project.find(params[:project_id])
    end

    def user_email
        params.require(:email).permit(:author_email)
    end

    def read_template
        data = File.read(Rails.root.join('lib/commits.txt'))
        return JSON.parse(data)
        #JSON.pretty_generate(JSON.parse(data))
    end

    def read_diffs
        data = File.read(Rails.root.join('lib/diffs.txt'))
        return JSON.parse(data)
    end

end
