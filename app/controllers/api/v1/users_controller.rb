require 'rest-client'

class Api::V1::UsersController < ApplicationController
    before_action :set_project, only: [:index]

    # GET /projects/1/commits
    def index
        begin
            response = RestClient.get ENV['BASE_URL'] + "contributors?rid=#{@project.git_id}"
            render json: response.body, status: :ok 
        rescue RestClient::ExceptionWithResponse => e
            render json: e.response
        end
    end


    private
    def set_project
        @project = Project.find(params[:project_id])
    end
    
end