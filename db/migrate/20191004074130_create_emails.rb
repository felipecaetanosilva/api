class CreateEmails < ActiveRecord::Migration[6.0]
  def change
    create_table :emails do |t|
      t.integer :projectId
      t.string :emailId
      t.string :senderName
      t.string :senderEmail
      t.timestamp :timestampReceived
      t.text :subject
      t.string :url
      t.string :replyto

      t.timestamps
    end
  end
end
